var badge = document.getElementById("cart-count");

function initialCart() {
  var initial_cart = {i1: 0, i2: 0};
  sessionStorage.setItem("cart", JSON.stringify(initial_cart));
}

var _cart = sessionStorage.getItem("cart")
if (!_cart) {
  initialCart();
}

function clearCart() {
  initialCart();
  badge.innerHTML = 0;
}

function getCartSum() {
  var val = 0;
  var cart = sessionStorage.getItem("cart");
  cart = JSON.parse(cart);

  for (var key in cart) {
    val += cart[key];
  }
  return val.toString();;
}

badge.innerHTML = getCartSum();

function addToCart(item_code) {
  var cart = sessionStorage.getItem("cart");
  cart = JSON.parse(cart);
  cart[item_code] += 1;
  sessionStorage.setItem("cart", JSON.stringify(cart));

  badge.innerHTML = getCartSum();
}

var items = {
  i1: {value : 57000, name: "RX 6900 XT"},
  i2: {value : 73000, name: "GTX 3090"},
};

function displayItems(show_check_out) {
  var cart = sessionStorage.getItem("cart");
  cart = JSON.parse(cart);
  var card_list = document.getElementById("cart-list");
  var total = 0;
  for (key in cart) {
    var item = cart[key];
    if (item) {
      var li = document.createElement("li");
      li.className = "list-group-item"
      var div = document.createElement("div");

      div.className = "d-flex justify-content-between"
      
      var name = document.createElement("p");
      name.innerHTML = items[key].name;
      div.append(name);
      
      var item_count = document.createElement("p");
      item_count.innerHTML = items[key].value;
      div.append(item_count);

      var item_count = document.createElement("p");
      item_count.innerHTML = item;
      div.append(item_count);

      var item_total = document.createElement("p");
      item_total.innerHTML = items[key].value * item;
      div.append(item_total);
      
      card_list.append(div)
      total += items[key].value * item;
    }
  }
  var cart_container = document.getElementById("cart-container");

  if (total) {
    var div = document.createElement("div");
    div.className = "d-flex justify-content-end"
    
    var grand_total = document.createElement("p");
    grand_total.innerHTML = total;
    div.append(grand_total);

    cart_container.append(div);
    
    if (show_check_out) {
      var checkout = document.createElement("a");
      checkout.className = "btn btn-success";
      checkout.innerHTML = "Checkout";
      checkout.href = "./check_out.html"
      cart_container.append(checkout);  
    }

  } else {
    var p = document.createElement("p");
    p.innerHTML = "Cart is empty";
    cart_container.append(p);
  }
}